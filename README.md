# Splash
An infinite list of images from Unsplash built using React Native.

<div align="center">![2014-10-22 11_35_09](https://thumbs.gfycat.com/GrizzledRapidGull-size_restricted.gif)</div>

## Generating the release APK
Run the following in a terminal:
- `git clone git@gitlab.com:vitalii.bukreev/react-native-app.git`
- `npm install`
- in root directory of project: `cd android`
- next: `./gradlew bundleRelease`
- and for testing the release build: `react-native run-android --variant=release`

Build is located under: `android/app/build/outputs/apk/release/app-release.apk`


